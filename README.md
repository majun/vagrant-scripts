Vagrant scripts
===============

apache2conf
-----------

Change atribute value in section.

**Ansible example:**

```
- name: set apache2 AllowOverride to All
  shell: php {{ vagrant_root }}/fixtures/scripts/apache2conf --backup --file /etc/httpd/conf/httpd.conf --check 'AllowOverride All' --search 'AllowOverride \w+' --replace 'AllowOverride All' --sec-start '<Directory "/vagrant_repos">' --sec-end '</Directory>'
  sudo: yes
  notify: restart apache2
  tags: apache

```

### TODO

- don't work regexp in `check` and `search` option
